﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace miniSupermarket
{
    public partial class transactions : Form
    {
        database mydb;
        int res = 0;
        
        MySqlConnection myconnect;
        MySqlCommand mycommand;
        DataTable dTable,dTable1,dTable2;
        string con;
        public transactions()
        {
            InitializeComponent();
            mydb = new database();
            con = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=minisupermarket";
            myconnect = new MySqlConnection(con);
            mycommand = new MySqlCommand();  
        }

        private void transactions_Load(object sender, EventArgs e)
        {
            try
            {
                myconnect.Open();
                ////attendant Name

                string query = "select id,fullname from attendants";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter = new MySqlDataAdapter();
                myAdapter.SelectCommand = mycommand;
                dTable = new DataTable();
                myAdapter.Fill(dTable);
                comboBoxAttendantsName.DataSource = dTable;
                comboBoxAttendantsName.DisplayMember = "fullname";
                comboBoxAttendantsName.ValueMember = "id";

                ///buyers Name
                string query1 = "select id,first_name from buyers";
                mycommand.CommandText = query1;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter1 = new MySqlDataAdapter();
                myAdapter1.SelectCommand = mycommand;
                dTable1 = new DataTable();
                myAdapter1.Fill(dTable1);
                comboBoxBuyersName.DataSource = dTable1;
                comboBoxBuyersName.DisplayMember = "first_name";
                comboBoxBuyersName.ValueMember = "id";

                ///products
                string query2 = "select id,product_name from products";
                mycommand.CommandText = query2;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter2 = new MySqlDataAdapter();
                myAdapter2.SelectCommand = mycommand;
                dTable2 = new DataTable();
                myAdapter2.Fill(dTable2);
                comboBoxProductName.DataSource = dTable2;
                comboBoxProductName.DisplayMember = "product_name";
                comboBoxProductName.ValueMember = "id";
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            
            int value = Convert.ToInt32(comboBoxAttendantsName.SelectedValue);
            int value1 = Convert.ToInt32(comboBoxBuyersName.SelectedValue);
            int value2 = Convert.ToInt32(comboBoxProductName.SelectedValue);
            
            string quanty = textBoxQuantity.Text;
                if (!int.TryParse(quanty, out res))
                    {
                        MessageBox.Show("Please enter a value in the quantity box");
                        return;
                    }
                int quantity = Convert.ToInt32(quanty);

            string amt = textBoxAmount.Text;
                if (!int.TryParse(amt, out res))
                    {
                        MessageBox.Show("Please enter a value in the amount box");
                            return;
                    }

                int amount = Convert.ToInt32(amt);
            mydb.transactions(value, value1, value2, quantity, amount);
            
        }

        private void newTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newBuyer mynewbuyer = new newBuyer();
            mynewbuyer.Show();
        }

        private void newTransactionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are here already!");
        }

        private void newSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newSupplier mynewsupplier = new newSupplier();
            mynewsupplier.Show();
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            categories mycat = new categories();
            mycat.Show();
        }

        private void newSupplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            supplies mysupplies = new supplies();
            mysupplies.Show();
        }

        private void newProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            products myproducts = new products();
            myproducts.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxViewAttendantsName.Visible = true;
            textBoxViewBuyersName.Visible = true;
            textBoxViewProductBought.Visible = true;
            textBoxViewAmountPaid.Visible = true;
            textBoxViewQuantityBought.Visible = true;


            textBoxViewAmountPaid.Text = textBoxAmount.Text;
            textBoxViewQuantityBought.Text = textBoxQuantity.Text;
            textBoxViewAttendantsName.Text = comboBoxAttendantsName.Text;
            textBoxViewBuyersName.Text = comboBoxBuyersName.Text;
            textBoxViewProductBought.Text = comboBoxProductName.Text;  
           }

        private void newAttendantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            attendants myattendants = new attendants();
            myattendants.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 myform = new Form1();
            this.Hide();
            myform.Show();
        }
        
    }
}
