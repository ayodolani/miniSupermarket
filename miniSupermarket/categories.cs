﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miniSupermarket
{
    public partial class categories : Form
    {
        database mydb;
        public categories()
        {
            InitializeComponent();
            mydb = new database();
        }

        private void newBuyerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newBuyer mynewbuyer = new newBuyer();
            mynewbuyer.Show();
        }

        private void newTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            transactions mytransact = new transactions();
            mytransact.Show();
        }

        private void newSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newSupplier mynewsupplier = new newSupplier();
            mynewsupplier.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBoxCategory.Text;

            mydb.newCategory(name);
        }

        private void newCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are here!!!");
        }

        private void newSupplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            supplies mysupplies = new supplies();
            mysupplies.Show();
        }

        private void newProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            products myproducts = new products();
            myproducts.Show();
        }

        private void newAttendantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            attendants myattendants = new attendants();
            myattendants.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 myform = new Form1();
            this.Hide();
            myform.Show();
        }
    }
}
