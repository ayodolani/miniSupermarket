﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace miniSupermarket
{
    public partial class products : Form
    {
        database mydb;
        MySqlConnection myconnect;
        MySqlCommand mycommand;
        DataTable dTable;
        string con;
        public products()
        {
            InitializeComponent();
            mydb = new database();
            con = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=minisupermarket";
            myconnect = new MySqlConnection(con);
            mycommand = new MySqlCommand();  
        }
        private void newTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            transactions mytransact = new transactions();
            mytransact.Show();
        }

        private void newProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are Here!!");
        }

        private void newSupplierToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            newSupplier mynewsupplier = new newSupplier();
            mynewsupplier.Show();
        }

        private void newCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            categories mycat = new categories();
            mycat.Show();
        }

        private void newBuyerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newBuyer mynewbuyer = new newBuyer();
            mynewbuyer.Show();
        }

        private void newSupplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            supplies mysupplies = new supplies();
            mysupplies.Show();
        }

        private void products_Load(object sender, EventArgs e)
        {
            try
            {
                myconnect.Open();
                ////Category Name
                string query = "select id,name from categories";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter = new MySqlDataAdapter();
                myAdapter.SelectCommand = mycommand;
                dTable = new DataTable();
                myAdapter.Fill(dTable);
                comboBoxCategory.DataSource = dTable;
                comboBoxCategory.DisplayMember = "name";
                comboBoxCategory.ValueMember = "id";
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int category_name = Convert.ToInt32(comboBoxCategory.SelectedValue);
            string product_name = textBoxproductName.Text;
            int price = Convert.ToInt32(textBoxPrice.Text);
            string description = richTextBoxDescription.Text;

            mydb.category(category_name,product_name,price,description);
        }

        private void newAttendantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            attendants myattendants = new attendants();
            myattendants.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 myform = new Form1();
            this.Hide();
            myform.Show();
        }
    }
}
