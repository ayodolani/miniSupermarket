﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace miniSupermarket
{
    public partial class supplies : Form
    {
        database mydb;
        MySqlConnection myconnect;
        MySqlCommand mycommand;
        DataTable dTable3, dTable4;
        string con;
        public supplies()
        {
            InitializeComponent();
            mydb = new database();
            con = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=minisupermarket";
            myconnect = new MySqlConnection(con);
            mycommand = new MySqlCommand();
        }

        private void newBuyerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newBuyer mynewbuyer = new newBuyer();
            mynewbuyer.Show();
        }

        private void newCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            categories mycat = new categories();
            mycat.Show();
        }

        private void newTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            transactions mytransact = new transactions();
            mytransact.Show();
        }

        private void newSupplyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are Here!!");
        }

        private void newSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            newSupplier mynewsupplier = new newSupplier();
            mynewsupplier.Show();
        }

        private void supplies_Load(object sender, EventArgs e)
        {
            try
            {
                myconnect.Open();
                
                ////Product Name

                string query3 = "select id,product_name from products";
                mycommand.CommandText = query3;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter3 = new MySqlDataAdapter();
                myAdapter3.SelectCommand = mycommand;
                dTable3 = new DataTable();
                myAdapter3.Fill(dTable3);
                comboBoxProductName.DataSource = dTable3;
                comboBoxProductName.DisplayMember = "product_name";
                comboBoxProductName.ValueMember = "id";

                ///Suppliers Name
                string query4 = "select id,business_name from suppliers";
                mycommand.CommandText = query4;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter4 = new MySqlDataAdapter();
                myAdapter4.SelectCommand = mycommand;
                dTable4 = new DataTable();
                myAdapter4.Fill(dTable4);
                comboBoxSupplierName.DataSource = dTable4;
                comboBoxSupplierName.DisplayMember = "business_name";
                comboBoxSupplierName.ValueMember = "id";
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int product_name = Convert.ToInt32(comboBoxProductName.SelectedValue);
            int supplier_name = Convert.ToInt32(comboBoxSupplierName.SelectedValue);

            mydb.supplies(product_name, supplier_name);
        }

        private void newProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            products myproducts = new products();
            myproducts.Show();
        }

        private void newAttendantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            attendants myattendants = new attendants();
            myattendants.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 myform = new Form1();
            this.Hide();
            myform.Show();
        }
    }
}
